var stimuli_data = null;

$(document).ready(
    function(){
        stimuli_data = (function(){
                            var data_url = "data/stimuli-list.json";
                            var batteries = [];
                            var me = {};
                            $.getJSON(data_url, function(data){
                                          me.batteries = data.batteries;
                                          me.practice = data.practice;
                                          start_test();
                                      });
                            return me;
                        })();
    });

function getFile(sentence) {
    return "audio/" + sentence + ".ogg";
}

function bindToAudioEnd(audioElement, callback){
    $(audioElement).bind('ended', function(){
                                 audioElement.unbind('ended');
                             callback();
                             });
}

function mark_good(fn){
    $("#main").html($("#main").html() + "<br/>" + fn);
}

function mark_bad(fn){
    $("#main").html($("#main").html() + "<br/><strike>" + fn + "</strike>");
}


function play(fn_list){
    if(fn_list.length == 0){
	mark_good("all done");
    }
    else {
	$('#audio').attr('src', fn_list[0]);
	$('#audio').attr('autoplay', 'true');
	var timeout = window.setTimeout(function(){
	    $("audio").unbind('ended');
	    console.log("timedout");
	    mark_bad(fn_list[0]);
	    play(fn_list.slice(1));
	}, 1000 * 8);
	bindToAudioEnd($("#audio"), function(){
	    mark_good(fn_list[0]);
	    play(fn_list.slice(1));
	    window.clearTimeout(timeout);
	});
    }
}

function start_test(){
    stims = [];
    for(var i = 0; i < stimuli_data["practice"].length; i += 1){
	stims.push(getFile(stimuli_data["practice"][i]));
    }
    for(var i = 0; i < stimuli_data["batteries"].length; i += 1){
	var battery = stimuli_data["batteries"][i];
	for(var j = 0; j < stimuli_data["batteries"][i].length; j += 1){
	    stims.push(getFile(stimuli_data["batteries"][i][j]));
	}
    }
    play(stims);
}
