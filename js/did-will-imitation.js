var stimuli_data = null;

$(document).ready(
    function(){
        stimuli_data = (function(){
                            var data_url = "data/stimuli-list.json";
                            var batteries = [];
                            var me = {};
                            $.getJSON(data_url, function(data){
                                          me.batteries = data.batteries;
                                          me.practice = data.practice;
                                          draw_buttons();
                                      });
                            return me;
                        })();
    });

function bindToSpacebar(callback){
    $(window).bind('keypress', function(e){
                       if (e.charCode == 32) {
                           $(window).unbind('keypress');
                           callback();
                       }
                   });
}

function bindToAudioEnd(audioElement, callback){
    $(audioElement).bind('ended', function(){
                                 audioElement.unbind('ended');
                             callback();
                             });
}

function getFile(sentence) {
    return "audio/" + sentence + ".ogg";
}

function finish(){
    $('.page-header h1').html("That's all!").show(500);
    $('#main').html("Thanks for participating in the experiment.").show(500);
}

function write(header, secondary, hLen, sLen){
    if (typeof(hLen) === 'undefined'){
        hLen = 500;
    }
    if (typeof(sLen) === 'undefined'){
        sLen = 700;
    }

    $('.page-header h1').html(header).show(hLen,
      function(){
          $('#main').html(secondary).show(sLen);
          });
}

function hideText(callback){;
    $('.page-header h1').hide(500);
    $('#main').hide(500, callback);
}

function practice(batteryNum, sentenceNum){
    if (typeof(sentenceNum) === 'undefined'){
        sentenceNum = 0;
        write("This is a practice round",
              "<p>When you press the spacebar you will hear a sentence. " +
             "Repeat the sentence aloud. When you're done speaking, press the " +
             "spacebar, and repeat." +
              " </p> That's all there is to it. " +
             "<hr>Press the spacebar when you're ready to start.");
        bindToSpacebar(function(){
                           practice(batteryNum, 0);
                           hideText();
                       });
    } else{
        var filename = getFile(stimuli_data.practice[sentenceNum]);
        $('#audio').attr('src', filename);
        $('#audio').attr('autoplay', 'true');
        bindToAudioEnd($('#audio'),
                       function(){
                           write("Repeat what you just heard.",
                                 "Press spacebar when you're done");
                           bindToSpacebar(
                               function(){
                                   if (sentenceNum ==
                                       stimuli_data.practice.length - 1){
                                       write("We're finished with the practice.",
                                             "Press spacebar to start the real thing.");
                                       bindToSpacebar(function(){
                                                          hideText(function(){
                                                                   start(batteryNum);});
                                                      });
                                   } else{
                                       hideText();
                                       practice(batteryNum, sentenceNum + 1);
                                   }
                               });
                       });
        }
}

function start(batteryNum, trialNum){
    if (typeof(trialNum) === 'undefined')
        trialNum = 0;
    var battery = stimuli_data.batteries[batteryNum];
    var sentence = battery[trialNum];
    var filename = getFile(sentence);
    $('#audio').attr('src', filename);
    $('#audio').attr('autoplay', 'true');
    bindToAudioEnd($('#audio'),
        function(){
            if (trialNum == battery.length - 1){
                finish();
            }
            else {
                write("Repeat what you just heard.",
                      "Press spacebar when you're done");

                bindToSpacebar(function(){
                                   $('.page-header h1').hide(500);
                                   $('#main').hide(500);
                                   start(batteryNum, trialNum + 1);
                               });
                }
            });
}

function draw_buttons(){
    var buttons, nums = [];
    for (var i = 0; i < stimuli_data.batteries.length; i += 1)
        nums.push(i + 1);
    buttons = nums.map(function(n){
                            return "<li><button class='btn' value=" + n +
                                "> Battery " + n + "</button></li>";
                        });
    $("#batteryPick h2").html("Pick a Battery");
    $('#batteryButtons').append(buttons.join(""));
    $('#batteryButtons button').click(
        function(button, x){
            var val = $(this).attr('value');
            $('#batteryPick').hide(
                500,
                function(){
                    $('.page-header > h1').html('Welcome to the Experiment');
                    $('#main').html('<h2>Press the spacebar to begin.</h2>');
                });
            bindToSpacebar(function(){
                               $('.page-header h1').hide(500);
                               $('#main').hide(500, function(){practice(val -1)});
                           });
        });
}
