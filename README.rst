====================
 Did/Will Imitation
====================

This is a simple HTML5 stimuli presenter testing participants' ability to repeat
sentences they hear. It's a single-page webapp so there's no server
necessary. Just open index.html in a modern web browser.

Here's what happens:

1. Participant hears a prompt.
2. Participant repeats prompt into tape recorder (which is rolling the whole
   time).
3. Participant presses button to indicate they're ready for the next prompt.

That's it.

The application does not collect any data.
